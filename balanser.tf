resource "yandex_vpc_network" "default" { # импортировал так как уже существовало
    description = "Auto-created network"
    folder_id   = var.folder_id
    name        = "network"
}

resource "yandex_vpc_subnet" "subnet_network_1" { # импортировал так как уже существовало
    description    = "Auto-created default subnet for zone ru-central1-c in network"
    folder_id      = var.folder_id
    name           = "network-ru-central1-c"
    network_id     = yandex_vpc_network.default.id 
    v4_cidr_blocks = [
        "10.130.0.0/24",
    ]
    zone           = "ru-central1-c"
}

resource "yandex_vpc_subnet" "subnet_network_2" { # импортировал так как уже существовало
    description    = "Auto-created default subnet for zone ru-central1-b in network"
    folder_id      = var.folder_id
    name           = "network-ru-central1-b"
    network_id     = yandex_vpc_network.default.id
    v4_cidr_blocks = [
        "10.129.0.0/24",
    ]
    zone           = "ru-central1-b"
}

resource "yandex_vpc_subnet" "subnet_network_3" { # импортировал так как уже существовало
    description    = "Auto-created default subnet for zone ru-central1-a in network"
    folder_id      = var.folder_id
    name           = "network-ru-central1-a"
    network_id     = yandex_vpc_network.default.id
    v4_cidr_blocks = [
        "10.128.0.0/24",
    ]
    zone           = "ru-central1-a"
}

resource "yandex_iam_service_account" "terraform" {
  name = "terraform"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  folder_id = var.folder_id
  role      = "editor"
  members   = [
    "serviceAccount:${yandex_iam_service_account.terraform.id}",
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "alb-editor" {
  folder_id = var.folder_id
  role      = "alb.editor"
  members   = [
    "serviceAccount:${yandex_iam_service_account.terraform.id}",
  ]
}

resource "yandex_compute_image" "netology_image" {
  source_family = "ubuntu-2204-lts"
}

resource "yandex_compute_instance_group" "group1" {
  name                 = "group1"
  folder_id            = var.folder_id
  service_account_id   = yandex_iam_service_account.terraform.id
  instance_template {
    platform_id        = "standard-v2"
    service_account_id = yandex_iam_service_account.terraform.id
    resources {
      memory           = 2
      cores            = 2
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = yandex_compute_image.netology_image.id
        type     = "network-ssd"
        size     = 15
      }
    }

    metadata = { 
        user-data = "${file("./meta.yml")}"
    }

    network_interface {
      network_id         = yandex_vpc_network.default.id
      subnet_ids         = [yandex_vpc_subnet.subnet_network_2.id,yandex_vpc_subnet.subnet_network_3.id]
      nat                = true
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["ru-central1-a", "ru-central1-b"]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  application_load_balancer {
    target_group_name = "group1"
  }
}

resource "yandex_alb_backend_group" "group-back" {
  name                     = "group-back"

  http_backend {
    name                   = "backend-1"
    port                   = 80
    target_group_ids       = [yandex_compute_instance_group.group1.application_load_balancer.0.target_group_id]
    healthcheck {
      timeout              = "10s"
      interval             = "2s"
      healthcheck_port     = 80
      http_healthcheck {
        path               = "/"
      }
    }
  }
}

resource "yandex_alb_http_router" "netology_router" {
  name   = "netology-router"
}

resource "yandex_alb_virtual_host" "netology-host" {
  name           = "netology-host"
  http_router_id = yandex_alb_http_router.netology_router.id
  route {
    name = "netology-router"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.group-back.id
      }
    }
  }
}

#################################################################################################
resource "yandex_alb_load_balancer" "balancer" {
  name               = "balancer"
  network_id         = yandex_vpc_network.default.id

  allocation_policy {
    location {
      zone_id   = "ru-central1-b"
      subnet_id = yandex_vpc_subnet.subnet_network_2.id
    }

    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.subnet_network_3.id
    }
  }

    listener {
      name = "listener-http"
      endpoint {
        ports = [
          80,
        ]
        address {
          external_ipv4_address {
            address = "${yandex_vpc_address.ip-addr.external_ipv4_address[0].address}"
          }
        }
      }
      http {
        redirects {
          http_to_https = true
        }
      }
    }

    listener {
      name = "listener-https"
      endpoint {
        ports = [
          443,
          ]
        address {
          external_ipv4_address {
            address = "${yandex_vpc_address.ip-addr.external_ipv4_address[0].address}"
          }
        }
      }
      tls {
        default_handler {
          certificate_ids = [
            var.cert_id, 
              ]
          http_handler {
            allow_http10   = false
            http_router_id = yandex_alb_http_router.netology_router.id
          }
        }
        sni_handler {
          name         = "netology-sni"
          server_names = [
            "devops-sre.ru",
              ]
            handler {
              certificate_ids = [
                var.cert_id,
                  ]
              http_handler {
                allow_http10   = false
                http_router_id = yandex_alb_http_router.netology_router.id 
              }
            }
        }
      }
    }
}

resource "yandex_vpc_address" "ip-addr" {
  name = "balancer-ip"
  folder_id = var.folder_id
  external_ipv4_address {
    ddos_protection_provider = "qrator"
    zone_id = "ru-central1-a"
  }
}

resource "yandex_dns_zone" "zone" { # создает зону и добавляет записи SOA и NS
    folder_id        = var.folder_id
    name             = "devops-sre"
    private_networks = [
        yandex_vpc_network.default.id,
    ]
    public           = true
    zone             = "devops-sre.ru."
}

 resource "yandex_dns_recordset" "rs-1" { # заменить
   zone_id = yandex_dns_zone.zone.id
   name    = "@"
   ttl     = 600
   type    = "A"
   data = [yandex_vpc_address.ip-addr.external_ipv4_address[0].address]
}