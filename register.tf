terraform {
  required_providers {
    yandex = {
        source = "yandex-cloud/yandex" # загружает модуль яндекса из registry.terraform.io (нужен vpn) или воспользоваться зеркалом yandex
        version = "= 0.83.0"
    }
  }
}

provider "yandex" {
    token = var.token                       # токен ЛК яндекса (выдается на год, можно отозвать)
    cloud_id = var.cloud_id                 # идентификатор облака
    folder_id = var.folder_id               # идентификатор директории в облаке (берется из адресной строки)
    zone = "ru-central1-a"                  # ЦОД зона доступности яндекс (должно совпадать с resource в main.tf)
  
}