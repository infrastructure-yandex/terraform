resource "yandex_compute_image" "netology_image_vm" {
  source_family = "ubuntu-2204-lts"
}

#### VM 1 Prometheus ####

resource "yandex_vpc_address" "ip-prom" {
  name = "prometheus"
  folder_id = var.folder_id
  external_ipv4_address {
    zone_id = "ru-central1-b"
  }
}

resource "yandex_compute_instance" "prometheus" {
  name                 = "prometheus"
  folder_id            = var.folder_id
  service_account_id   = yandex_iam_service_account.terraform.id
  zone                 = "ru-central1-b"

    resources {
      memory           = 2
      cores            = 2
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = yandex_compute_image.netology_image_vm.id
        type     = "network-ssd"
        size     = 15
      }
    }

    metadata = { 
        user-data = "${file("./meta.yml")}"
    }

    network_interface {
      subnet_id          = "${yandex_vpc_subnet.subnet_network_2.id}"
      nat = true
      nat_ip_address     = "${yandex_vpc_address.ip-prom.external_ipv4_address[0].address}"
    }

    provisioner "remote-exec" {
      inline = [
        "echo '${var.ssh}' | sudo tee /root/.ssh/authorized_keys > /dev/null",
        "sudo sed -i '0,/#PermitRootLogin No/s//PermitRootLogin yes/' /etc/ssh/sshd_config",
        "sudo systemctl restart sshd"
      ]
      connection {
        type = "ssh"
        user = var.user
        private_key = file("~/.ssh/id_rsa")
        host = "${yandex_vpc_address.ip-prom.external_ipv4_address[0].address}"
      }
    }
}

 resource "yandex_dns_recordset" "rs-2" {
   zone_id = yandex_dns_zone.zone.id
   name    = "prometheus"
   ttl     = 600
   type    = "A"
   data = [yandex_vpc_address.ip-prom.external_ipv4_address[0].address]
}

#### VM 2 Grafana ####

resource "yandex_vpc_address" "ip-grafana" {
  name = "grafana"
  folder_id = var.folder_id
  external_ipv4_address {
    zone_id = "ru-central1-b"
  }
}

resource "yandex_compute_instance" "grafana" {
  name                 = "grafana"
  folder_id            = var.folder_id
  service_account_id   = yandex_iam_service_account.terraform.id
  zone                 = "ru-central1-b"

    resources {
      memory           = 2
      cores            = 2
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = yandex_compute_image.netology_image_vm.id
        type     = "network-ssd"
        size     = 15
      }
    }

    metadata = { 
        user-data = "${file("./meta.yml")}"
    }

    network_interface {
      subnet_id          = "${yandex_vpc_subnet.subnet_network_2.id}"
      nat = true
      nat_ip_address     = "${yandex_vpc_address.ip-grafana.external_ipv4_address[0].address}"
    }

    provisioner "remote-exec" {
      inline = [
        "echo '${var.ssh}' | sudo tee /root/.ssh/authorized_keys > /dev/null",
        "sudo sed -i '0,/#PermitRootLogin No/s//PermitRootLogin yes/' /etc/ssh/sshd_config",
        "sudo systemctl restart sshd"
      ]
      connection {
        type = "ssh"
        user = var.user
        private_key = file("~/.ssh/id_rsa")
        host = "${yandex_vpc_address.ip-grafana.external_ipv4_address[0].address}"
      }
    }
}

 resource "yandex_dns_recordset" "rs-3" {
   zone_id = yandex_dns_zone.zone.id
   name    = "grafana"
   ttl     = 600
   type    = "A"
   data = [yandex_vpc_address.ip-grafana.external_ipv4_address[0].address]
}