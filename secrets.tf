variable "token" {
    type = string
    default = "secret"
    sensitive = true
}

variable "cloud_id" {
    type = string
    default = "b1gr7tmblvijg2rq8t1o"
}

variable "folder_id" {
    type = string
    default = "b1g9sat33q5t4nm7o4kt"
}

variable "cert_id" {
    type = string
    default = "fpq64nuu2pltupmuugjl"
}

variable "image" {
    type = string
    default = "fd8gle1jaqma4saejife"
}

variable "user" {
    type = string
    default = "netology"
}

variable "ssh" {
    type = string
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHAHrQsyS4E0Elf86H5beaBezNNLTG03h7oBbFpof036MHLR6qBnabCscK4MdZ4RM6oilk+vDL7QI2Vz4l5uFrkTnFUuanfLOZJdCyfCeaVMe5Imbub5FV8lds2vjk5uA81eE/izDnb/PvK2AZCF7Ezx8TUHYnwPGtHTQ8Qwv9BkgAqPTfHhNrliIRSUWy8OHa54DRs6+oXWrGtgLRcOFP2sFkG3wUv6Vlmw/gwlmKie/HlcJWoQ0K0M9SXnHbqiYAOEIA4Tprfz+R7/VkLTUffCuGk5ZHVsvSPfPGLXpWb2Pl96VeVvLl8BbXo0bvtJgpjj650ZGykXtI/w0l3evX dmitry@MacBook-Pro-Dmitrij.local"
  
}